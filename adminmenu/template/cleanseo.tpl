{if !empty($notice)}
    <div class="alert alert-info">{$notice}</div>
{/if}
{if !empty($error)}
    <div class="alert alert-danger">{$error}</div>
{/if}

<div id="payment">
    <div id="tabellenLivesuche">
        <form name="seocleaner" id="seocleaner" method="post" action="">
            {$jtl_token}
            <input name="clean" type="hidden" value="1"/>

            {if isset($cCleanlog) && $cCleanlog|strlen > 0}
                <div class="alert alert-success">{$cCleanlog}</div>
            {/if}
                <div class="panel panel-default">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>{__('Table')}</th>
                            <th>{__('Outdated rows in tseo')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $cStatus.table as $table => $count}
                            <tr>
                                <td class="tleft">
                                    {$table}
                                </td>
                                <td class="tcenter">
                                    <div class="progress" style="margin:0;">
                                        {if $count > 0}
                                            <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="min-width: 2em; width: {100/$cStatus.total*$count}%">
                                                <span class="sr-only">{$count}</span>{$count}
                                            </div>
                                        {/if}
                                    </div>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <td>{__('Total')}</td>
                            <td>{$cStatus.total}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <button name="submitDelete" type="submit" class="btn btn-warning">
                    <i class="fa fa-chain-broken"></i> {__('Clean up')}
                </button>
        </form>
    </div>
</div>
