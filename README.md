JTL SEOCleaner
======

Bereinigt SEO-Leichen (Veraltete Einträge in tseo, zu denen kein Eintrag in der jew. Tabelle zu dem Fremdschlüssel existiert)

# Changelog

## v2.0.0
- Shop5-Kompatibilität

## v102
- Shop4-Anpassungen
- Performance-Optimierungen

## v101
- Shop4-Anpassungen

## v101
- Initiales Relaese
