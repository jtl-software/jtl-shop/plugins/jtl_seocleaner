<?php declare(strict_types=1);

namespace Plugin\jtl_seocleaner;

use JTL\Helpers\Form;
use JTL\Helpers\Request;
use JTL\Plugin\Bootstrapper;
use JTL\Smarty\JTLSmarty;

/**
 * Class Bootstrap
 * @package Plugin\jtl_seocleaner
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritDoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        $notice  = '';
        $error   = '';
        $tables  = [
            'kKategorie'   => 'tkategorie',
            'kHersteller'  => 'thersteller',
            'kArtikel'     => 'tartikel',
            'kLink'        => 'tlink',
            'kSuchanfrage' => 'tsuchanfrage',
            'kMerkmalWert' => 'tmerkmalwert',
            'kNews'        => 'tnews',
        ];
        $cleaner = new Cleaner($this->getDB());
        if (Request::postInt('clean') === 1 && Form::validateToken()) {
            $cleanLog = $cleaner->cleanUp($tables);
            $notice   = (\count($cleanLog) > 0)
                ? __('The following items have been deleted:')
                : __('No outdated rows found.');
            $smarty->assign('cCleanlog', \implode('<br />', $cleanLog));
        }

        return $smarty->assign('cStatus', $cleaner->showSeo($tables))
            ->assign('notice', $notice)
            ->assign('error', $error)
            ->fetch($this->getPlugin()->getPaths()->getAdminPath() . 'template/cleanseo.tpl');
    }
}
