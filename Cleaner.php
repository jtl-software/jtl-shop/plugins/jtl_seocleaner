<?php declare(strict_types=1);

namespace Plugin\jtl_seocleaner;

use JTL\DB\DbInterface;
use JTL\DB\ReturnType;

/**
 * Class Cleaner
 * @package Plugin\jtl_seocleaner
 */
class Cleaner
{
    /**
     * @var DbInterface
     */
    private $db;

    /**
     * Cleaner constructor.
     * @param DbInterface $db
     */
    public function __construct(DbInterface $db)
    {
        $this->db = $db;
    }

    /**
     * @param array $tables
     * @return array
     */
    public function cleanUp(array $tables): array
    {
        $log = [];
        foreach ($tables as $key => $table) {
            $results = $this->db->queryPrepared(
                'SELECT cKey, kKey, cSeo FROM tseo 
                    WHERE cKey = :key 
                        AND kKey NOT IN (SELECT ' . $key . ' FROM ' . $table . ')',
                ['key' => $key],
                ReturnType::ARRAY_OF_OBJECTS
            );
            if (\count($results) === 0) {
                continue;
            }
            $this->db->queryPrepared(
                'DELETE FROM tseo
                WHERE cKey = :key
                    AND kKey NOT IN (SELECT ' . $key . ' FROM ' . $table . ')',
                ['key' => $key],
                ReturnType::DEFAULT
            );
            foreach ($results as $result) {
                $log[] = \sprintf(__('Item %s with key %s = %d deleted.'), $result->cSeo, $result->cKey, $result->kKey);
            }
        }

        return $log;
    }

    /**
     * @param array $tables
     * @return array
     */
    public function showSeo(array $tables): array
    {
        $status = ['table' => [], 'total' => 0];
        foreach ($tables as $key => $table) {
            $result                  = $this->db->queryPrepared(
                'SELECT COUNT(cKey) AS cnt
                    FROM tseo 
                    WHERE cKey = :key 
                       AND kKey NOT IN (SELECT ' . $key . ' FROM ' . $table . ')',
                ['key' => $key],
                ReturnType::SINGLE_OBJECT
            );
            $status['table'][$table] = (int)$result->cnt;
            $status['total']        += (int)$result->cnt;
        }
        $tags                     = (int)$this->db->query(
            'SELECT COUNT(*) AS cnt 
                FROM tseo 
                WHERE cKey = \'kTag\'',
            ReturnType::SINGLE_OBJECT
        )->cnt;
        $status['table']['ttags'] = $tags;
        $status['total']         += $tags;

        $surveys                     = (int)$this->db->query(
            'SELECT COUNT(*) AS cnt 
                FROM tseo 
                WHERE cKey = \'kUmfrage\'',
            ReturnType::SINGLE_OBJECT
        )->cnt;
        $status['table']['tumfrage'] = $surveys;
        $status['total']            += $surveys;

        return $status;
    }
}
